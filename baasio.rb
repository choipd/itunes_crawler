require 'json'
require 'rest-client'

class Baasio

	def initialize(args)
		@endpoint = "https://api.baas.io/5590d950-ea9c-11e2-b34b-06f4fe0000b5/d949726f-176d-11e3-b248-06ebb80000ba"
		@client_id = "YXA62UlybxdtEeOySAbruAAAug"
		@client_secret = "YXA65iPUnkAxRemV9whvQ5LZVJ0KIj0"		

		@access_token = ""

		self.authenticate
	end
	
	def authenticate
		response = RestClient.post "#{@endpoint}/token", 
				{grant_type:"client_credentials", 
					client_id:@client_id, 
					client_secret:@client_secret}.to_json, 
					content_type: "json", 
					accept: "json"

		@access_token = JSON.parse(response)["access_token"]
	end

	def getApplication
		response = RestClient.get "#{@endpoint}", {Authorization: "Bearer #{@access_token}",accept: "json"}
	end

	def get(resource)
		response = RestClient.get "#{@endpoint}/#{resource}"
	end

	def post(resource, param)
		response = RestClient.post "#{@endpoint}/#{resource}",
				param.to_json, 
				Authorization:"Bearer #{@access_token}", 
				content_type:"json", 
				accept:"json"
	end
end

