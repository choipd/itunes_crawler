require 'nokogiri'
require 'open-uri'
require 'thread'
require 'thwait'
load 'baasio.rb'

$ts = Time.now.to_i
OpenSSL::SSL::VERIFY_PEER = OpenSSL::SSL::VERIFY_NONE

def start
  baas = Baasio.new(nil)

  url = 'http://www.apple.com/euro/itunes/charts/podcasts/top10podcasts.html'

  doc = Nokogiri::HTML(open(url))  
  
  categories = doc.xpath("//select/option[@value!='']")

  threads = []
  categories.each do |category|
   threads << Thread.new {
      p "category: #{category.content} thread started!"
      chart = getChart "url" => 'http://www.apple.com' + category['value'], "category" => category.content
      baas.post "charts", chart
   }
  end
  ThreadsWait.all_waits(*threads)
end

def getChart(param)
  chart = Hash.new
  doc = Nokogiri::HTML(open(param["url"]))

  countries = doc.css("div.box")

  countries.each do |country|
    p "[#{country.css("h2").first.content}]"
    podcasts = Array.new
    country.css("li").each_with_index do |podcast, idx|
      data = find_feed(podcast.css("a").first.attr('href'))
      podcast_data = {
        "rank" => idx + 1,
        "author" => podcast.css("span").first.content,
        "title" => podcast.css("strong").first.content,
        "rss" => data[0],
        "cover_image" => data[1]
      }
      podcasts << podcast_data
    end
    chart[country.css("h2").first.content] = podcasts
    chart["version"] = $ts 
    chart["category"] = param["category"]
  end
  chart
end


def find_feed(url)
  begin
    podcast_page = Nokogiri::XML(open_via_iTunes(url))
    stringTags = podcast_page.xpath("//string")

    has_feed_page = (stringTags.empty? ? podcast_page : Nokogiri::HTML(open_via_iTunes(stringTags.last.content)))

    feed = has_feed_page.css("*[feed-url]").first.attr("feed-url")
    image = has_feed_page.css("*[src-swap-high-dpi]").first.attr("src-swap-high-dpi")
    [feed, image]
  rescue
    "Error : Can't be found feed_url"
  end
end

def open_via_iTunes(url)
  open(url, 'User-Agent' => "iTunes/9.1.1")
end